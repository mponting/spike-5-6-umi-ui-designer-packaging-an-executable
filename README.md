| **Spike ID**        | B5 & B6                                                                                  | **Time Needed** | 3 hours |
|---------------------|------------------------------------------------------------------------------------------|-----------------|---------|
| **Title**           | Base Spike 5 – UMI UI Designer & Base Spike 6 – Packaging an Executable                  |
| **Personnel**       | Mathew Ponting (blueprogrammer) | Vikram Saran (vsaran) – (Spike 6)                      |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/spike-5-6-umi-ui-designer-packaging-an-executable> |

Introduction
============

B5
--

In this spike, we construct on top of our Spike 4 a GUI system. A GUI
(Generated User Interface) is the objects and images that appear on the
screen that are not in the world. This can include a Heads-Up Display
where normally in a First Person Shooter is were health and bullet total
are displayed, A Main Menu and a Pause Menu. All three of these are
constructed in this Spike.

B6
--

In this spike, the programmer is tasked to construct a .exe file
exported from Unreal Engine using the project that have built between
both Spike 5 and Spike 6. This will cover what exactly is needed to be
done when you want to export a game to a developer build or final build
which will be released in public.

Goal
====

B5
--

Create a Heads-Up-Display that displays information of the game. Create
a Main Menu and a Pause Menu.

B6
--

Understand the process of readying a project to package as a exe file
for Windows (64 bit) and Understand what cooking the project exactly
does.

TRT (Technology, Resources and Tools)
=====================================

| **Resource**                                                                                       | **Needed/Recommended** |
|----------------------------------------------------------------------------------------------------|------------------------|
| Create a Basic HUD - <https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/1/index.html> | Needed                 |
| Create a Pause Menu - <https://www.youtube.com/watch?v=gSt-7jY0t80>                                | Needed                 |
| Packaging a Windows 64-Bit Program in Unreal - <https://www.youtube.com/watch?v=P878gUw9l-E>       | Recommended            |

Tasks
=====

B5
--

1.  Create 3 new Blueprint Widget files. Name them each HUD, Pause
    and MainMenu.

2.  Construct a basic HUD with 2 text boxes that can easily be seen.

    1.  Make a function that updates the score text UI with the
        player’s score.

    2.  Make reference to the player controller where the score is added
        and run that function.

    3.  Make the other text function say that the game is over but make
        it hidden. Then make a function that turns it visible and call
        it when the game is over.

3.  With the player controller, make a reference to the hud type and
    make it appear on the screen. This should now let us see the HUD on
    the screen.

4.  Create a brand new level that is empty and make it show the Main
    Menu Widget. You want to connect it and make it appear in the
    level’s blueprint code.

    1.  Create a Text Field with the name of the game, And also with
        details about the game (You can put whatever you like in there).

    2.  Wrap the text fields in a group of choosing (So its much easier
        for them to toggle appear or disappear).

    3.  Create 3 buttons that are New Game, Info and Quit. Wrap them in
        a group as well.

        1.  New Game will load the level that you created in using the
            Spike 4 report.

        2.  Info will toggle off the main menu and show the info.

        3.  Quit quits the player out of the application.

    4.  After the main menu, if you haven’t in Spike 4, create a new
        blueprint file that has the player pawn you created in spike 4.

    5.  Make a new InputAction for Pause. Then in the BP Pawn, create a
        new Pause and UnPause functions. (Watch The Video in
        the resources).

    6.  With the widget, make reference to the pause menu.

    7.  Create the pause menu.

        1.  Create a button that calls the UnPause function.

        2.  Create a button that loads the MainMenu level.

        3.  Create a button that quits the game.

    8.  On anything that requires to press buttons, make sure that the
        mouse cursor is enabled on the mouse cursor. Disable it when it
        is no longer needed (Beginning of Level, Unpausing the game).

B6
--

1.  Open the project’s packaging settings and set them to make sure that
    the main menu is loaded first and the correct Player Controller,
    Game Mode and Player Pawns are set up correctly.

2.  Cook your project and then package it into a Windows (64 bit) file.

3.  Test to see if it works!

Discovered
==========

B5
--

-   GUI’s are a good way to display information to the user.

-   You can use the GUI to design a basic overlay screen for the game.

-   Pausing and Unpausing the game requires you to make sure the focus
    is on the game and UI respectively. In addition, it is wise to when
    the main menu starts, to make sure the cursor is being shown.

B6
--

-   Content Cooking in Unreal Engine allows the programmer or project
    owner to prepare their project for the platform they are about to
    place it on.

-   You must make sure your code is fully correct (not just compiled)
    before you export. This if not met can make your game fail
    at exporting.

Open Issues and Risks
=====================

Issues
------

B6
--

If your project gets an exit code of 5 and fails to export as a project,
this can mean various of errors could have occur. When preparing this
spike, I had issues as 1 C++ file that was in the project directory was
not programmed correctly. I had used an override by mistake and that
would always prevent me from failing. However, ExitCode 5 could mean
anything which includes not setting up visual studio correctly, having a
plugin installed, not having the correct developer kit install, and much
more. This error took a long time to fix as we could not pinpoint the
exact error of what was occurring.

Risks
-----

| Risk                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Risk Level |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| Another issue is that Visual Studio doesn’t seem to notice and compiles normally when *override* is being used incorrectly (if you override a function but have a typo in the name, this will compile and run but it will not *package*). This is the reason why the issue mentioned above was not solved very quickly and was unable to quickly pinpoint where the error was occurring. A Solution to this would be install a plugin called Resharper (available for free for students) which will allow you to see the capabilities of Visual Studio more powerful. You could also use Visual Assist X to achieve this same result. | Moderate   |

Recommendations
===============

B5
--

-   When launching a scene with only a gui in it, do it from the Level
    Blueprint otherwise if it’s a pause menu or HUD, do it from the
    player controller.

-   Get Familiar with Anchors, they can make your huds look very nice
    and well formatted. And not all over the place.

B6
--

-   I would recommend packaging your game at least once during a day of
    development, that way if any errors occur with exporting, you can
    back track that day to solve what caused the exporting problem.

-   Package your game at the beginning of the project, that way you
    confirm that the project will compile and properly export. This can
    be handy to note during a project’s development. You could also tag
    in your repository when a project was exported correctly.

-   Always set the game to default the main menu and make sure the game
    state, player controller and game mode are set up correctly in the
    packaging settings.

