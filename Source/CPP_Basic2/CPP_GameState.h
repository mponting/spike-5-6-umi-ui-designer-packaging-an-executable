// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "CPP_GameState.generated.h"


UCLASS()
class CPP_BASIC2_API ACPP_GameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	ACPP_GameState();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient)
	bool FreezePlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int score;

	//Unreal Functions//
	UFUNCTION(BlueprintCallable)
	void TogglePlayerFreeze();

	UFUNCTION()
	bool GetFreezePlayerStatus();

	UFUNCTION(BlueprintCallable)
	void AddOneToScore();
};
