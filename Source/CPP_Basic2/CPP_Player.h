// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "CPP_Player.generated.h"

UCLASS()
class CPP_BASIC2_API ACPP_Player : public APawn
{
	GENERATED_BODY()

public:
	//Sets default values for this pawn's properties
	ACPP_Player();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Physics")
	void OnHit(AActor * SelfActor, UPrimitiveComponent * OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere)
	USceneComponent* OurVisibleComponent;

	//Move Functions (Input)//
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		void Move_XAxis(float AxisValue);
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		void Move_YAxis(float AxisValue);
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		void AddSpeed(float AxisValue);
	
	//InputVariables
	FVector CurrentVelocity; //Floating Vector
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Transient, Category = "Velocity")
	float Speed;
};
