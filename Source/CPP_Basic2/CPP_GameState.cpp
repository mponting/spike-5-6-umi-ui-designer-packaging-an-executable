// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_Basic2.h"
#include "CPP_GameState.h"

ACPP_GameState::ACPP_GameState() {
	FreezePlayers = false;
	score = 0;
}

void ACPP_GameState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_GameState::TogglePlayerFreeze()
{
	if (FreezePlayers == true) {
		FreezePlayers = false;
		return;
	} else {
		FreezePlayers = true;
		return;
	}
}

bool ACPP_GameState::GetFreezePlayerStatus() {
	return FreezePlayers;
}

void ACPP_GameState::AddOneToScore()
{
	score++;
}
