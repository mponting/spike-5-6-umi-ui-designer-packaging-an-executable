// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_Basic2.h"
#include "CPP_Player.h"
#include "CPP_GameState.h"


// Sets default values
ACPP_Player::ACPP_Player()
{
	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a dummy root component we can attach things to.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	
	// Create a camera and a visible object
	UCameraComponent* OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));
	
	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(FVector(-250.0f, 0.0f, 250.0f));
	OurCamera->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));
	OurVisibleComponent->SetupAttachment(RootComponent);

	//Setup Box Collision
	UBoxComponent* BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box"));
	BoxCollider->SetupAttachment(RootComponent);
	BoxCollider->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics, true);
	//BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &BoxCollider::OnBeginOverlap);
}

// Called when the game starts or when spawned
void ACPP_Player::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ACPP_GameState* MyGameState = GetWorld()->GetGameState<ACPP_GameState>();
	bool FreezePlayers = MyGameState->GetFreezePlayerStatus();

	//Movement Ticks//
	if (!CurrentVelocity.IsZero()) {
		if (MyGameState) {
			if (FreezePlayers == false) {

				// ternary operator
				float actualSpeed = Speed > 1.0 ? Speed : 1.0f;

				FVector NewLoc;
				NewLoc = GetActorLocation() + (CurrentVelocity * DeltaTime) * actualSpeed;

				/*if (Speed == 0.0f) {
					NewLoc = GetActorLocation() + (CurrentVelocity * DeltaTime);
				}
				else {
					NewLoc = GetActorLocation() + (CurrentVelocity * DeltaTime) * Speed;
				}*/
				SetActorLocation(NewLoc);
			}
		}
	}
}

void ACPP_Player::OnHit(AActor * SelfActor, UPrimitiveComponent * OtherActor, FVector NormalImpulse, const FHitResult & Hit)
{
}

void ACPP_Player::Move_XAxis(float AxisValue) {
	CurrentVelocity.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}
void ACPP_Player::Move_YAxis(float AxisValue) {
	CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}

void ACPP_Player::AddSpeed(float AxisValue)
{
	Speed = AxisValue * 10.0f;
}




